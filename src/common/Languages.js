/** @format */

import LocalizedStrings from "react-native-localization";

export default new LocalizedStrings({
  en: {
    Exit: "Exit",
    ExitConfirm: "Are you sure you want to exit this app",
    YES: "YES",
    OK: "OK",
    ViewMyOrders: "View My Oders",
    CANCEL: "CANCEL",
    Confirm: "Confirm",

    // Scene's Titles
    Home: "Home",
    Intro: "Intro",
    Product: "Product",
    Cart: "Cart",
    WishList: "WishList",
    InputPhone: "Please input your phone",

    // Home
    products: "products",

    // TopBar
    ShowFilter: "Sub Categories",
    HideFilter: "Hide",
    Sort: "Sort",
    textFilter: "Recent",

    // Category
    ThereIsNoMore: "There is no more product to show",

    // Product
    AddedtoCart: "Added item to Cart",
    AddtoCart: "Add to Cart",
    AddtoWishlist: "Add to Wishlist",
    ProductVariations: "Variations",
    NoVariation: "This product don't have any variation",
    AdditionalInformation: "Description",
    NoProductDescription: "No Product Description",
    ProductReviews: "Reviews",
    NoReview: "This product don't have any reviews ...yet",
    BUYNOW: "BUY NOW",
    ProductLimitWaring: "You can't add more than 10 product",
    EmptyProductAttribute: "This product don't have any attributes",
    ProductFeatures: "Features",
    ErrorMessageRequest: "Can't get data from server",
    NoConnection: "No internet connection",
    ProductRelated: "You May Also Like",

    // Cart
    NoCartItem: "There is no product in cart",
    Total: "Total",
    EmptyCheckout: "Sorry, you can't check out an empty cart",
    RemoveCartItemConfirm: "Remove this product from cart?",
    MyCart: "Cart",
    Order: "Order",
    ShoppingCart: "Shopping Cart",
    ShoppingCartIsEmpty: "Your Cart is Empty",
    Delivery: "Delivery",
    AddProductToCart: "Add a product to the shopping cart",
    TotalPrice: "Total Price:",
    YourDeliveryInfo: "Your delivery info:",
    ShopNow: "Shop Now",
    YourChoice: "Your wishlist:",
    YourSale: "Your Sale:",
    SubtotalPrice: "Subtotal Price:",
    BuyNow: "Buy Now",
    Items: "items",
    Item: "item",
    ThankYou: "Thank you",
    FinishOrderCOD: "You can use to number of order to track shipping status",
    FinishOrder:
      "Thank you so much for your purchased, to check your delivery status please go to My Orders",
    NextStep: "Next Step",
    ConfirmOrder: "Confirm Order",
    RequireEnterAllFileds: "Please enter all fields",
    Error: "Error",
    InvalidEmail: "Invalid email address",
    Finish: "Finish",

    // Wishlist
    NoWishListItem: "There is no item in wishlist",
    MoveAllToCart: "Add all to cart",
    EmptyWishList: "Empty wishlist",
    EmptyAddToCart: "Sorry, the wishlist is empty",
    RemoveWishListItemConfirm: "Remove this product from wishlist?",
    CleanAll: "Clean All",

    // Sidemenu
    SignIn: "Log In",
    SignOut: "Log Out",
    GuestAccount: "Guest Account",
    CantReactEmailError:
      "We can't reach your email address, please try other login method",
    NoEmailError: "Your account don't have valid email address",
    EmailIsNotVerifiedError:
      "Your email address is not verified, we can' trust you",
    Shop: "Shop",
    News: "News",
    Contact: "Contact us",
    Setting: "Setting",
    Login: "Login",
    Logout: "Logout",
    Category: "Category",
    Adverts: "Classofied Ads",
    VendorDashboard: "Vendor Dashboard",

    // Checkout
    Checkout: "Checkout",
    ProceedPayment: "Proceed Payment",
    Purchase: "Purchase",
    CashOnDelivery: "Cash on Delivery",
    Paypal: "Paypal",
    Stripe: "Stripe",
    woocommerce_dpo: "woocommerce_dpo",
    maxicashgateway: "maxicashgateway",
    CreditCard: "Credit Card",
    PaymentMethod: "Payment Method - Not select",
    PaymentMethodError: "Please select your payment method",
    PayWithCoD: "Your purchase will be pay when goods were delivered",
    PayWithPayPal: "Your purchase will be pay with PayPal",
    PayWithStripe: "Your purchase will be pay with Stripe",
    ApplyCoupon: "Apply",
    CouponPlaceholder: "COUPON CODE",
    APPLY: "APPLY",
    Back: "Back",
    CardNamePlaceholder: "Name written on card",
    BackToHome: "Back to Home",
    OrderCompleted: "Your order was completed",
    OrderCanceled: "Your order was canceled",
    OrderFailed: "Something went wrong...",
    OrderCompletedDesc: "Your order id is ",
    OrderCanceledDesc:
      "You have canceled the order. The transaction has not been completed",
    OrderFailedDesc:
      "We have encountered an error while processing your order. The transaction has not been completed. Please try again",
    OrderTip:
      'Tip: You could track your order status in "My Orders" section from side menu',
    Payment: "Payment",
    Complete: "Complete",
    EnterYourFirstName: "Enter your First Name",
    EnterYourLastName: "Enter your Last Name",
    EnterYourEmail: "Enter your email",
    EnterYourPhone: "Enter your phone",
    EnterYourAddress: "Enter your address",
    CreateOrderError: "Cannot create new order. Please try again later",
    AccountNumner: "Account number",
    CardHolderName: "Cardholder Name",
    ExpirationDate: "Expiration Date",
    SecurityCode: "CVV",

    // myorder
    OrderId: "Order ID",
    MyOrder: "My Orders",
    NoOrder: "You don't have any orders",
    OrderDate: "Order Date: ",
    OrderStatus: "Status: ",
    OrderPayment: "Payment method: ",
    OrderTotal: "Total: ",
    OrderDetails: "Show detail",
    ShippingAddress: "Shipping Address:",
    Refund: "Refund",

    PostDetails: "Post Details",
    FeatureArticles: "Feature articles",
    MostViews: "Most views",
    EditorChoice: "Editor choice",

    // settings
    Settings: "Settings",
    BASICSETTINGS: "BASIC SETTINGS",
    Language: "Language",
    INFO: "INFO",
    About: "About us",
    changeRTL: "Switch RTL",

    // language
    AvailableLanguages: "Available Languages",
    SwitchLanguage: "Switch Language",
    SwitchLanguageConfirm: "Switch language require an app reload, continue?",
    SwitchRtlConfirm: "Switch RTL require an app reload, continue?",

    // about us
    AppName: "WakeupProgram",
    AppDescription: "Africa's Largest Marketplace",
    AppContact: " Contact us at: wakeupkingfoundation@gmail.com",
    AppEmail: " Email: wakeupkingfoundation@gmail.com",
    AppCopyRights: "© Wakeupking 2020",

    // contact us
    contactus: "Contact us",

    // form
    NotSelected: "Not selected",
    EmptyError: "This field is empty",
    DeliveryInfo: "Delivery Info",
    FirstName: "First Name",
    LastName: "Last Name",
    Address: "Address",
    City: "Town/City",
    State: "State",
    NotSelectedError: "Please choose one",
    Postcode: "Postcode",
    Country: "Country",
    Email: "Email",
    Phone: "Phone Number",
    Note: "Note",

    // search
    Search: "Search",
    SearchPlaceHolder: "Search product by name",
    NoResultError: "Your search keyword did not match any products.",
    Details: "Details",

    // filter panel
    Categories: "Categories",

    // sign up
    profileDetail: "Profile Details",
    firstName: "First name",
    lastName: "Last name",
    accountDetails: "Account Details",
    username: "Username",
    email: "Email",
    generatePass: "Use generate password",
    password: "Password",
    signup: "Sign Up",

    // filter panel
    Loading: "LOADING...",
    welcomeBack: "Welcome back! ",
    seeAll: "Show All",

    // Layout
    cardView: "Card ",
    simpleView: "List View",
    twoColumnView: "Two Column ",
    threeColumnView: "Three Column ",
    listView: "List View",
    default: "Default",
    advanceView: "Advance ",
    horizontal: "Horizontal ",

    couponCodeIsExpired: "This coupon code is expired",
    invalidCouponCode: "This coupon code is invalid",
    remove: "Remove",
    reload: "Reload",
    applyCouponSuccess: "Congratulations! Coupon code applied successfully ",
    applyCouponFailMin: `This minimum spend for this coupon is `,
    applyCouponFailMax: `This maximum spend for this coupon is `,

    OutOfStock: "OUT OF STOCK",
    ShippingType: "Shipping method",

    // Place holder
    TypeFirstName: "Type your first name",
    TypeLastName: "Type your last name",
    TypeAddress: "Type address",
    TypeCity: "Type your town or city",
    TypeState: "Type your state",
    TypeNotSelectedError: "Please choose one",
    TypePostcode: "Type postcode",
    TypeEmail: "Type email (Ex. acb@gmail.com), ",
    TypePhone: "Type your phone number",
    TypeNote: "Note",
    TypeCountry: "Select country",
    SelectPayment: "Select Payment method",
    close: "CLOSE",
    noConnection: "NO INTERNET ACCESS",

    // user profile screen
    AccountInformations: "Account Informations",
    PushNotification: "Push notification",
    DarkTheme: "Dark Theme",
    Privacy: "Privacy policies",
    SelectCurrency: "Select currency",
    Name: "Name",
    Currency: "Currency",
    Languages: "Languages",

    GetDataError: "Can't get data from server",
    UserOrEmail: "Username or email",
    Or: "Or",
    FacebookLogin: "Facebook Login",
    DontHaveAccount: "Don't have an account?",

    // Horizontal
    featureProducts: "Feature Products",
    bagsCollections: "Bags Collections",
    womanBestSeller: "Woman Best Seller",
    manCollections: "Man Collections",

    // Modal
    Select: "Select",
    Cancel: "Cancel",
    Guest: "Guest",

    LanguageName: "English",

    // review
    vendorTitle: "Vendor",
    comment: "Leave a review",
    yourcomment: "Your comment",
    placeComment:
      "Tell something about your experience or leave a tip for others",
    writeReview: "Review",
    thanksForReview:
      "Thanks for the review, your content will be verify by the admin and will be published later",
    errInputComment: "Please input your content to submit",
    errRatingComment: "Please rating to submit",
    send: "Send",

    termCondition: "Term & Condition",
    Subtotal: "Subtotal",
    Discount: "Discount",
    Shipping: "Shipping",
    Taxes: "Taxes",
    Recents: "Recents",
    Filters: "Filters",
    Princing: "Pricing",
    Filter: "Filter",
    ClearFilter: "Clear Filter",
    ProductCatalog: "Product Catalog",
    ProductTags: "Product Tags",
    AddToAddress: "Add to Address",
    SMSLogin: "SMS Login",
    OrderNotes: "Order Notes",

    CanNotLogin: "Can not login, something was wrong!",
    PleaseCompleteForm: "Please complete the form!",
    ServerNotResponse: "Server doesn't response correctly",
    CanNotRegister: "Can't register user, please try again."
  },
  ar: {
    Exit: "خروج",
    ExitConfirm: "هل تود الخروج من التطبيق",
    YES: "نعم",
    OK: "نعم",
    ViewMyOrders: "أستعرض طلبي",
    CANCEL: "إلغاء",
    Confirm: "تأكيد",

    // Scene's Titles
    Home: "الرئيسة",
    Intro: "مقدمة",
    Product: "منتج",
    Cart: "عربة التسوق",
    WishList: "الرغبات",

    // Home
    products: "منتجات",

    // TopBar
    ShowFilter: "الفئات الفرعية",
    HideFilter: "إخفاء",
    Sort: "فرز",
    textFilter: "الأخيرة",

    // Category
    ThereIsNoMore: "لا يوجد منتجات لإظهارها",

    // Product
    AddedtoCart: "إضافة عنصر إلى السلة",
    AddtoCart: "أضف إلى السلة",
    AddtoWishlist: "أضف إلى قائمة الرغبات",
    ProductVariations: "الاختلافات",
    NoVariation: "هذا المنتج ليس لديهم أي اختلاف",
    AdditionalInformation: "معلومات",
    NoProductDescription: "لا يوجد وصف",
    ProductReviews: "التعليقات",
    NoReview: "لا توجد مراجعات لهذا المنتج",
    BUYNOW: "اشتر الآن",
    ProductLimitWaring: "لا يمكن اضافة اكثر من هذا العدد",
    EmptyProductAttribute: "لم يتم اضافة اي صفات لهذا المنتج",
    ProductFeatures: "المميزات",
    ErrorMessageRequest: "لا يمكن الحصول على البيانات من الخادم",
    NoConnection: "انت غير متصل بالإنترنت",
    ProductRelated: "ربما يعجبك أيضا",
    // Cart
    NoCartItem: "لا يوجد منتجات في العربة",
    Total: "مجموع",
    EmptyCheckout: "عذراً، لا يمكن الاستمرار مع عربة فارغة",
    RemoveCartItemConfirm: "إزالة هذا المنتج من سلة المشتريات؟",
    MyCart: "عربة التسوق",
    Order: "طلب",
    ShoppingCart: "عربة التسوق",
    ShoppingCartIsEmpty: "عربة التسوق فارغة",
    Delivery: "التوصيل",
    AddProductToCart: "إضافة منتج إلى عربة التسوق",
    TotalPrice: "السعر الكلي:",
    YourDeliveryInfo: "معلومات الإستلام:",
    ShopNow: "تسوق الآن",
    YourChoice: "عربتك:",
    YourSale: "مبيعاتك:",
    SubtotalPrice: "السعر الإجمالي:",
    BuyNow: "اشتر الآن",
    Items: "العناصر",
    Item: "عنصر",
    ThankYou: "شكراً لك",
    FinishOrderCOD: "يمكنك استخدام هذا الرقم لتتبع شحنتك",
    FinishOrder:
      "شكراً لإتمام عملية الشراء, للتحقق من حالة الطلب الرجاء مراجعة طلباتي",
    NextStep: "الخطوة التالية",
    ConfirmOrder: "أكد الطلب",
    RequireEnterAllFileds: "الرجاء إدخال جميع الحقول",
    Error: "خطأ",
    InvalidEmail: "البريد الإلكتروني غير صحيح",
    Finish: "إنهاء",

    // Wishlist
    NoWishListItem: "لا يوجد منتجات في قائمة الرغبات",
    MoveAllToCart: "اضف الكل للسلة",
    EmptyWishList: "القائمة فارغة",
    EmptyAddToCart: "عذرا، قائمة الرغبات فارغة",
    RemoveWishListItemConfirm: "إزالة هذا المنتج من قائمة الرغبات؟",
    CleanAll: "ازالة الكل",

    // Sidemenu
    SignIn: "تسجيل الدخول",
    SignOut: "خروج",
    GuestAccount: "زائر",
    CantReactEmailError: "هذا البريد غير موجود، جرب طريقة تسجيل دخول أخرى",
    NoEmailError: "البريد الإلكتروني المضاف غير صحيح",
    EmailIsNotVerifiedError:
      "لم يتم التحقق من بريدك الإلكتروني، لن يتم تفعيل حسابك",
    Shop: "المتجر",
    News: "الأخبار",
    Contact: "اتصل بنا",
    Setting: "الإعدادات",
    Login: "تسجيل الدخول",
    Logout: "تسجيل خروج",
    Category: "التصنيف",

    // Checkout
    Checkout: "الدفع",
    ProceedPayment: "متابعة الدفع",
    Purchase: "شراء",
    CashOnDelivery: "الدفع عن الاستلام",
    Paypal: "باي بال",
    Stripe: "شريط",
    CreditCard: "بطاقة ائتمان",
    PaymentMethod: "طريقة الدفع - غير محدد",
    PaymentMethodError: "يرجى تحديد طريقة الدفع",
    PayWithCoD: "يرجى تحديد طريقة الدفع",
    PayWithPayPal: "الدفع بواسطة Paypal",
    PayWithStripe: "سيتم دفع الشراء مع Stripe",
    ApplyCoupon: "اضف الكوبون",
    CouponPlaceholder: "رمز الكوبون",
    APPLY: "ارسل",
    Back: "العودة",
    CardNamePlaceholder: "الاسم مكتوب على البطاقة",
    BackToHome: "العودة لرئيسة",
    OrderCompleted: "تم الانتهاء من طلبك",
    OrderCanceled: "تم إلغاء طلبك",
    OrderFailed: "تم إلغاء طلبك",
    OrderCompletedDesc: "أجل معرف الخاص بك هو ",
    OrderCanceledDesc: "تم الغاء الطلب بناء على رغبتك",
    OrderFailedDesc: "حصل خطأ ولم تكتمل عملية الشراء",
    OrderTip:
      'إضاءة: يمكنك تتبع حالة الطلب في قسم "طلباتي" من القائمة الجانبية',
    Payment: "دفع",
    Complete: "دفع...",
    EnterYourFirstName: "أدخل اسمك الأول",
    EnterYourLastName: "أدخل اسم العائلة",
    EnterYourEmail: "أدخل بريدك الإلكتروني",
    EnterYourPhone: "أدخل هاتفك",
    EnterYourAddress: "أدخل عنوانك",
    CreateOrderError: "أدخل عنوانك...",
    AccountNumner: "رقم حساب",
    CardHolderName: "إسم صاحب البطاقة",
    ExpirationDate: "تاريخ إنتهاء الصلاحية",
    SecurityCode: "CVV",

    // myorder
    OrderId: "رقم التعريف الخاص بالطلب",
    MyOrder: "طلبي",
    NoOrder: "لا يوجد طلبات",
    OrderDate: "تاريخ الطلب: ",
    OrderStatus: "الحالة: ",
    OrderPayment: "طريقة الدفع او السداد: ",
    OrderTotal: "مجموع: ",
    OrderDetails: "أظهر تفاصيل",
    ShippingAddress: "عنوان الشحن:",
    Refund: "إعادة مال",

    PostDetails: "قراءة المزيد",
    FeatureArticles: "مقالات",
    MostViews: "الأكثر مشاهدة",
    EditorChoice: "من اختياراتنا",

    // settings
    Settings: "الإعدادات",
    BASICSETTINGS: "الإعدادات الأساسية",
    Language: "اللغو",
    INFO: "معلومات",
    About: "معلومات عنا",
    changeRTL: "تغيير الإتجاه",

    // language
    AvailableLanguages: "اللغات المتوفرة",
    SwitchLanguage: "تبديل اللغة",
    SwitchLanguageConfirm:
      "لتبديل اللغة سوف نقوم باعادة تحميل التطبيق، هل تود المتابعة؟",
    SwitchRtlConfirm:
      "لتغيير اتجاه التطبيق سوف نقوم بإعادة تحميل التطبيق, هل تود المتابعة؟",

    // about us
    AppName: "MSTORE",
    AppDescription: "رد القالب الأصلي ل كومومرس",
    AppContact: " الاتصال بنا على: mstore.io",
    AppEmail: " البريد الإلكتروني: support@mstore.io",
    AppCopyRights: "© MSTORE 2016",

    // contact us
    contactus: "اتصل بنا",

    // form
    NotSelected: "فضلاً اختر",
    EmptyError: "عبئ هذ الحقل",
    DeliveryInfo: "معلومات التسليم",
    FirstName: "الاسم الاول",
    LastName: "الكنية",
    Address: "عنوان",
    City: "المدينة / المدينة",
    State: "حالة",
    NotSelectedError: "إختر واحد من فضلك",
    Postcode: "الرمز البريدي",
    Country: "بلد",
    Email: "البريد الإلكتروني",
    Phone: "رقم الهاتف",
    Note: "ملاحظة",

    // search
    Search: "البحث",
    SearchPlaceHolder: "البحث عن المنتج بالاسم",
    NoResultError: "نأسف، لا توجد نتائج بحث للكلمات المستخدمة",
    Details: "التفاصيل",

    // filter panel
    Categories: "الاقسام",

    // sign up
    profileDetail: "تفاصيل الملف الشخصي",
    firstName: "الاسم الاول",
    lastName: "الكنية",
    accountDetails: "تفاصيل الحساب",
    username: "اسم المستخدم",
    email: "البريد الإلكتروني",
    generatePass: "أنشأ لي كلمة مرور",
    password: "كلمه السر",
    signup: "سجل",

    // filter panel
    Loading: "جار التحميل...",
    welcomeBack: "مرحبا بعودتك! ",
    seeAll: "استعراض الكل",

    // Layout
    cardView: "بطاقة",
    simpleView: "قائمة مبسطة",
    twoColumnView: "عمودين",
    threeColumnView: "ثلاثة أعمدة",
    listView: "قائمة عمودية",
    default: "افتراضي",
    advanceView: "اسلوب متقدم",
    horizontal: "أفقي ",

    couponCodeIsExpired: "لم يعد هذا الكوبون صالح",
    invalidCouponCode: "هذا الكوبون غير صحيح",
    remove: "إزالة",
    applyCouponSuccess: "تهانينا! تم تطبيق الكوبون بنجاح",
    reload: "إعادة تحميل",

    OutOfStock: "نفذت الكمية",
    ShippingType: "طريقة الشحن",

    // Place holder
    TypeFirstName: "اكتب اسمك الأول",
    TypeLastName: "اكتب اسمك الأخير",
    TypeAddress: "اكتب العنوان",
    TypeCity: "اكتب مدينتك أو مدينتك",
    TypeState: "اكتب الولاية أو المدينة",
    TypeNotSelectedError: "إختر واحد من فضلك",
    TypePostcode: "اكتب الرمز البريدي",
    TypeEmail: "اكتب بريد الكتروني صحيح مثل email@email.com: ",
    TypePhone: "اكتب رقم هاتفك",
    TypeNote: "ملاحظة",
    TypeCountry: "حدد دولتك",
    SelectPayment: "اختار طريقة الدفع",
    close: "أغلق",
    noConnection: "لا يوجد اتصال بالإنترنت",

    // user profile screen
    AccountInformations: "معلومات الحساب",
    PushNotification: "التنبيهات",
    DarkTheme: "موضوع الظلام",
    Privacy: "سياسة الخصوصية",
    SelectCurrency: "اختر العملة",
    Name: "اسم",
    Currency: "عملة",
    Languages: "اللغات",

    GetDataError: "لا يمكن الاتصال بالخادم",
    UserOrEmail: "اسم المستخدم أو البريد الالكتروني",
    Or: "أو",
    FacebookLogin: "تسجيل الدخول بواسطة الفيس بوك",
    DontHaveAccount: "لا تمتلك حساب؟",

    // Horizontal
    featureProducts: "منتجات مميزة",
    bagsCollections: "تشكيلة الحقائب",
    womanBestSeller: "افضل مبيعات منتجات المرأة",
    manCollections: "تشكيلة الرجل",

    // Modal
    Select: "تحديد",
    Cancel: "إلغاء",
    Guest: "زائر",

    LanguageName: "Arabic",

    // review
    vendorTitle: "بائع",
    comment: "ترك التعليق",
    yourcomment: "تعليقك",
    placeComment: "أخبر شيئًا عن تجربتك أو اترك نصيحة للآخرين",
    writeReview: "أكتب مراجعة",
    thanksForReview:
      "شكرًا على المراجعة ، سيتم التحقق من المحتوى الخاص بك بواسطة المشرف وسيتم نشره لاحقًا",
    errInputComment: "يرجى إدخال المحتوى الخاص بك لتقديمه",
    errRatingComment: "يرجى تصنيف لتقديم",
    send: "إرسال",

    termCondition: "الأحكام والشروط",
    Subtotal: "حاصل الجمع",
    Discount: "خصم",
    Shipping: "الشحن",
    Taxes: "الضرائب",
    Recents: "الأخيرة",
    Filters: "مرشحات",
    Princing: "التسعير",
    Filter: "منقي",
    ClearFilter: "مرشح واضح",
    ProductCatalog: "بيان المنتج",
    ProductTags: "علامات المنتج",
    AddToAddress: "أضف إلى العنوان",
    SMSLogin: "تسجيل الدخول عبر الرسائل القصيرة",
    OrderNotes: "ترتيب ملاحظات",

    CanNotLogin: "لا يمكن تسجيل الدخول ، كان هناك خطأ!",
    PleaseCompleteForm: "يرجى ملء النموذج!",
    ServerNotResponse: "الخادم لا يستجيب بشكل صحيح",
    CanNotRegister: "لا يمكن تسجيل المستخدم ، يرجى المحاولة مرة أخرى."
  },
  fr: {
    Exit: "Sortie",
    ExitConfirm: "Êtes-vous sûr de vouloir quitter cette application",
    YES: "OUI",
    OK: "D'accord",
    ViewMyOrders: "Voir mes commandes",
    CANCEL: "ANNULER",
    Confirm: "Confirmer",

    // Scene's Titles
    Home: "Accueil",
    Intro: "Intro",
    Product: "Produit",
    Cart: "Chariot",
    WishList: "Liste de souhaits",
    InputPhone: "Veuillez saisir votre téléphone",

    // Home
    products: "des produits",

    // TopBar
    ShowFilter: "Sous-catégories",
    HideFilter: "Cacher",
    Sort: "Trier",
    textFilter: "Récente",

    // Category
    ThereIsNoMore: "Il n'y a plus de produit à montrer",

    // Product
    AddedtoCart: "Article ajouté au panier",
    AddtoCart: "Ajouter au chariot",
    AddtoWishlist: "Ajouter à la liste de souhaits",
    ProductVariations: "Variations",
    NoVariation: "Ce produit n'a aucune variation",
    AdditionalInformation: "La description",
    NoProductDescription: "Aucune description de produit",
    ProductReviews: "Commentaires",
    NoReview: "Ce produit n'a pas encore d'avis ...",
    BUYNOW: "ACHETER MAINTENANT",
    ProductLimitWaring: "Vous ne pouvez pas ajouter plus de 10 produits",
    EmptyProductAttribute: "Ce produit n'a aucun attribut",
    ProductFeatures: "Caractéristiques",
    ErrorMessageRequest: "Impossible d'obtenir les données du serveur",
    NoConnection: "Pas de connexion Internet",
    ProductRelated: "Tu pourrais aussi aimer",

    // Cart
    NoCartItem: "Il n'y a pas de produit dans le panier",
    Total: "Totale",
    EmptyCheckout: "Désolé, vous ne pouvez pas vérifier un panier vide",
    RemoveCartItemConfirm: "Supprimer ce produit du panier?",
    MyCart: "Chariot",
    Order: "Ordre",
    ShoppingCart: "Panier",
    ShoppingCartIsEmpty: "Votre panier est vide",
    Delivery: "Livraison",
    AddProductToCart: "Ajouter un produit au panier",
    TotalPrice: "Prix ​​total:",
    YourDeliveryInfo: "Vos informations de livraison:",
    ShopNow: "Achetez maintenant",
    YourChoice: "Votre liste de souhaits:",
    YourSale: "Votre vente:",
    SubtotalPrice: "Prix ​​sous-total:",
    BuyNow: "Acheter maintenant",
    Items: "articles",
    Item: "article",
    ThankYou: "Je vous remercie",
    FinishOrderCOD: "Vous pouvez utiliser le numéro de commande pour suivre l'état de l'expédition",
    FinishOrder:
      "Merci beaucoup pour votre achat, pour vérifier l'état de votre livraison, veuillez vous rendre dans Mes commandes",
    NextStep: "L'étape suivante",
    ConfirmOrder: "Confirmer la commande",
    RequireEnterAllFileds: "Veuillez saisir tous les champs",
    Error: "Error",
    InvalidEmail: "Adresse e-mail invalide",
    Finish: "Finish",

    // Wishlist
    NoWishListItem: "Il n'y a aucun article dans la liste de souhaits",
    MoveAllToCart: "Tout ajouter au panier",
    EmptyWishList: "Liste de souhaits vide",
    EmptyAddToCart: "Désolé, la liste de souhaits est vide",
    RemoveWishListItemConfirm: "Supprimer ce produit de la liste de souhaits?",
    CleanAll: "Clean All",

    // Sidemenu
    SignIn: "Log In",
    SignOut: "Log Out",
    GuestAccount: "Guest Account",
    CantReactEmailError:
      "Nous ne pouvons pas atteindre votre adresse e-mail, veuillez essayer une autre méthode de connexion",
    NoEmailError: "Votre compte n'a pas d'adresse e-mail valide",
    EmailIsNotVerifiedError:
      "Votre adresse e-mail n'est pas vérifiée, nous pouvons vous faire confiance",
    Shop: "Shop",
    News: "News",
    Contact: "Contact us",
    Setting: "Setting",
    Login: "Login",
    Logout: "Logout",
    Category: "Category",
    Adverts: "Classofied Ads",
    VendorDashboard: "Vendor Dashboard",

    // Checkout
    Checkout: "Checkout",
    ProceedPayment: "Procéder au paiement",
    Purchase: "Achat",
    CashOnDelivery: "Cash on Delivery",
    Paypal: "Paypal",
    Stripe: "Stripe",
    woocommerce_dpo: "woocommerce_dpo",
    maxicashgateway: "maxicashgateway",
    CreditCard: "Credit Card",
    PaymentMethod: "Payment Method - Not select",
    PaymentMethodError: "Veuillez sélectionner votre mode de paiement",
    PayWithCoD: "Votre achat sera payé lors de la livraison des marchandises",
    PayWithPayPal: "Votre achat sera payé avec PayPal",
    PayWithStripe: "Votre achat sera payé avec Stripe",
    ApplyCoupon: "Apply",
    CouponPlaceholder: "COUPON CODE",
    APPLY: "APPLY",
    Back: "Back",
    CardNamePlaceholder: "Nom inscrit sur la carte",
    BackToHome: "Back to Home",
    OrderCompleted: "Votre commande est terminée",
    OrderCanceled: "Votre commande a été annulée",
    OrderFailed: "Something went wrong...",
    OrderCompletedDesc: "Your order id is ",
    OrderCanceledDesc:
      "Vous avez annulé la commande. La transaction n'est pas terminée",
    OrderFailedDesc:
      "Nous avons rencontré une erreur lors du traitement de votre commande. La transaction n'est pas terminée. Veuillez réessayer",
    OrderTip:
      'Tip: You could track your order status in "My Orders" section from side menu',
    Payment: "Payment",
    Complete: "Complete",
    EnterYourFirstName: "Entrez votre prénom",
    EnterYourLastName: "Entrez votre nom",
    EnterYourEmail: "Entrer votre Email",
    EnterYourPhone: "Entrez votre téléphone",
    EnterYourAddress: "Entrez votre adresse",
    CreateOrderError: "Impossible de créer une nouvelle commande. Veuillez réessayer plus tard",
    AccountNumner: "Account number",
    CardHolderName: "Cardholder Name",
    ExpirationDate: "Expiration Date",
    SecurityCode: "CVV",

    // myorder
    OrderId: "Order ID",
    MyOrder: "My Orders",
    NoOrder: "Vous n'avez aucune commande",
    OrderDate: "Date de commande:",
    OrderStatus: "Statut: ",
    OrderPayment: "Mode de paiement: ",
    OrderTotal: "Totale: ",
    OrderDetails: "Montrer les détails",
    ShippingAddress: "Adresse de livraison:",
    Refund: "Rembourser",

    PostDetails: "Détails de l'article",
    FeatureArticles: "Articles de fond",
    MostViews: "Les plus vus",
    EditorChoice: "Choix de l'éditeur",

    // settings
    Settings: "Settings",
    BASICSETTINGS: "BASIC SETTINGS",
    Language: "Language",
    INFO: "INFO",
    About: "About us",
    changeRTL: "Switch RTL",

    // language
    AvailableLanguages: "Available Languages",
    SwitchLanguage: "Changer de langue",
    SwitchLanguageConfirm: "Changer de langue nécessite un rechargement de l'application, continuer?",
    SwitchRtlConfirm: "Switch RTL require an app reload, continue?",

    // about us
    AppName: "WakeupProgram",
    AppDescription: "Africa's Largest Marketplace",
    AppContact: " Contact us at: wakeupkingfoundation@gmail.com",
    AppEmail: " Email: wakeupkingfoundation@gmail.com",
    AppCopyRights: "© Wakeupking 2020",

    // contact us
    contactus: "Contact us",

    // form
    NotSelected: "Non séléctionné",
    EmptyError: "Ce champ est vide",
    DeliveryInfo: "Informations de livraison",
    FirstName: "First Name",
    LastName: "Last Name",
    Address: "Address",
    City: "Town/City",
    State: "State",
    NotSelectedError: "Please choose one",
    Postcode: "Postcode",
    Country: "Country",
    Email: "Email",
    Phone: "Phone Number",
    Note: "Note",

    // search
    Search: "Search",
    SearchPlaceHolder: "Rechercher un produit par nom",
    NoResultError: "Votre mot clé de recherche ne correspond à aucun produit.",
    Details: "Détails",

    // filter panel
    Categories: "Categories",

    // sign up
    profileDetail: "Détails du profil",
    firstName: "First name",
    lastName: "Last name",
    accountDetails: "Détails du compte",
    username: "Username",
    email: "Email",
    generatePass: "Use generate password",
    password: "Password",
    signup: "Sign Up",

    // filter panel
    Loading: "LOADING...",
    welcomeBack: "Nous saluons le retour! ",
    seeAll: "Show All",

    // Layout
    cardView: "Card ",
    simpleView: "List View",
    twoColumnView: "Two Column ",
    threeColumnView: "Three Column ",
    listView: "List View",
    default: "Default",
    advanceView: "Advance ",
    horizontal: "Horizontal ",

    couponCodeIsExpired: "Ce code de réduction est expiré",
    invalidCouponCode: "Ce code de réduction n'est pas valide",
    remove: "Retirer",
    reload: "Recharger",
    applyCouponSuccess: "Toutes nos félicitations! Code de coupon appliqué avec succès ",
    applyCouponFailMin: `This minimum spend for this coupon is `,
    applyCouponFailMax: `This maximum spend for this coupon is `,

    OutOfStock: "EN RUPTURE DE STOCK",
    ShippingType: "Mode de livraison",

    // Place holder
    TypeFirstName: "Tapez votre prénom",
    TypeLastName: "Tapez votre nom",
    TypeAddress: "Saisissez l'adresse",
    TypeCity: "Tapez votre ville ou ville",
    TypeState: "Tapez votre état",
    TypeNotSelectedError: "Choisissez en un s'il vous plait",
    TypePostcode: "Tapez le code postal",
    TypeEmail: "Saisissez un e-mail (Ex. acb@gmail.com), ",
    TypePhone: "Tapez votre numéro de téléphone",
    TypeNote: "Note",
    TypeCountry: "Choisissez le pays",
    SelectPayment: "Sélectionnez le mode de paiement",
    close: "CLOSE",
    noConnection: "NO INTERNET ACCESS",

    // user profile screen
    AccountInformations: "Informations sur le compte",
    PushNotification: "Push notification",
    DarkTheme: "Dark Theme",
    Privacy: "Les politiques de confidentialité",
    SelectCurrency: "Select currency",
    Name: "Name",
    Currency: "Currency",
    Languages: "Languages",

    GetDataError: "Impossible d'obtenir les données du serveur",
    UserOrEmail: "Username or email",
    Or: "Or",
    FacebookLogin: "Facebook Login",
    DontHaveAccount: "Don't have an account?",

    // Horizontal
    featureProducts: "Produits caractéristiques",
    bagsCollections: "Bags Collections",
    womanBestSeller: "Woman Best Seller",
    manCollections: "Man Collections",

    // Modal
    Select: "Select",
    Cancel: "Cancel",
    Guest: "Guest",

    LanguageName: "French",

    // review
    vendorTitle: "Vendor",
    comment: "Leave a review",
    yourcomment: "Your comment",
    placeComment:
      "Tell something about your experience or leave a tip for others",
    writeReview: "Review",
    thanksForReview:
      "Merci pour l'examen, votre contenu sera vérifié par l'administrateur et sera publié plus tard",
    errInputComment: "Please input your content to submit",
    errRatingComment: "Please rating to submit",
    send: "Send",

    termCondition: "Term & Condition",
    Subtotal: "Subtotal",
    Discount: "Discount",
    Shipping: "Shipping",
    Taxes: "Taxes",
    Recents: "Recents",
    Filters: "Filters",
    Princing: "Pricing",
    Filter: "Filter",
    ClearFilter: "Clear Filter",
    ProductCatalog: "Product Catalog",
    ProductTags: "Product Tags",
    AddToAddress: "Add to Address",
    SMSLogin: "SMS Login",
    OrderNotes: "Order Notes",

    CanNotLogin: "Can not login, something was wrong!",
    PleaseCompleteForm: "Veuillez remplir le formulaire!",
    ServerNotResponse: "Le serveur ne répond pas correctement",
    CanNotRegister: "Impossible d'enregistrer l'utilisateur, veuillez réessayer."
  },
});
